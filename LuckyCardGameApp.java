import java.util.Scanner;
public class LuckyCardGameApp
{
	public static void main (String[] args)
	{
		Deck myDeck = new Deck();
		myDeck.shuffle();
		System.out.println("Welcome to card game... how many cards do you wish to remove");
		
		Scanner reader = new Scanner(System.in);
		
		int removeCards = -1;
		while (removeCards < 0 || removeCards > myDeck.length())
		{
				removeCards = reader.nextInt();
				if (removeCards < 0 || removeCards > myDeck.length()) 
				{
					System.out.println("You must enter a number 0-" + myDeck.length());
				}
        }
		
		for (int i = 0; i < removeCards; i++)
		{
			myDeck.drawTopCard();
		}

		System.out.println(myDeck.length());
		myDeck.shuffle();
		System.out.println(myDeck);
		
	}


}